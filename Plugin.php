<?php namespace Monologophobia\Company;

use App;
use Illuminate\Foundation\AliasLoader;
use Monologophobia\Company\Classes\UserRedirector;

use Backend\Facades\Backend;
use System\Classes\PluginBase;

/**
 * FileTransfer Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Company',
            'description' => 'Project Management, Banking, Invoicing, etc.',
            'author'      => 'Monologophobia',
            'icon'        => 'icon-briefcase'
        ];
    }

    public function registerPermissions()
    {
        return [
            'monologophobia.company.projects' => ['tab' => 'Company', 'label' => 'List, Amend, Create Projects'],
            'monologophobia.company.clients'  => ['tab' => 'Company', 'label' => 'List, Amend, Create Clients'],
            'monologophobia.company.salepurchasetypes'  => ['tab' => 'Company', 'label' => 'Amend Sale and Purchase Types'],
            'monologophobia.company.invoices'  => ['tab' => 'Company', 'label' => 'View Invoices'],
            'monologophobia.company.payments'  => ['tab' => 'Company', 'label' => 'Manage Payments'],
            'monologophobia.company.settings'    => ['tabl' => 'Company', 'label' => 'Change Company Settings']
        ];
    }

    public function registerNavigation()
    {
        return [
            'company' => [
                'label'       => 'Company',
                'url'         => Backend::url('monologophobia/company/projects'),
                'icon'        => 'icon-briefcase',
                'permissions' => ['monologophobia.company.*'],
                'order'       => 600,

                'sideMenu' => [
                    'projects' => [
                        'label'       => 'Projects',
                        'url'         => Backend::url('monologophobia/company/projects'),
                        'icon'        => 'icon-pencil-square',
                        'permissions' => ['monologophobia.company.projects']
                    ],
                    'clients' => [
                        'label'       => 'Clients',
                        'url'         => Backend::url('monologophobia/company/clients'),
                        'icon'        => 'icon-users',
                        'permissions' => ['monologophobia.company.clients']
                    ],
                    'invoices' => [
                        'label'       => 'Invoices',
                        'url'         => Backend::url('monologophobia/company/invoices'),
                        'icon'        => 'icon-file-pdf-o',
                        'permissions' => ['monologophobia.company.invoices']
                    ],
                    'payments' => [
                        'label'       => 'Payments',
                        'url'         => Backend::url('monologophobia/company/payments'),
                        'icon'        => 'icon-credit-card',
                        'permissions' => ['monologophobia.company.payments']
                    ]
                    /*'files' => [
                        'label'       => 'Shared Files',
                        'url'         => Backend::url('monologophobia/company/files'),
                        'icon'        => 'icon-files-o',
                        'permissions' => ['monologophobia.company.files']
                    ],
                    'saletypes' => [
                        'label'       => 'Sale Types',
                        'url'         => Backend::url('monologophobia/company/saletypes'),
                        'icon'        => 'icon-wrench',
                        'permissions' => ['monologophobia.company.salepurchasetypes']
                    ],
                    'purchasetypes' => [
                        'label'       => 'Purchase Types',
                        'url'         => Backend::url('monologophobia/company/purchasetypes'),
                        'icon'        => 'icon-wrench',
                        'permissions' => ['monologophobia.company.salepurchasetypes']
                    ]*/
                ]
            ]
        ];
    }

    public function registerComponents()
    {
        return [
           '\Monologophobia\Company\Components\PayInvoice' => 'payinvoice'
        ];
    }

    public function registerMailTemplates() {
        return [
            'monologophobia.company::invoices.invoice' => 'The email to send to a client who wants to be sent invoices on creation'
        ];
    }

    public function registerReportWidgets() {
        return [
            'Monologophobia\Company\ReportWidgets\NewProject' => [
                'label'   => 'New Quick Project',
                'context' => 'dashboard'
            ],
            'Monologophobia\Company\ReportWidgets\Overview' => [
                'label'   => 'Company Overview',
                'context' => 'dashboard'
            ],
            'Monologophobia\Company\ReportWidgets\Ongoing' => [
                'label'   => 'Ongoing Projects Overview',
                'context' => 'dashboard'
            ],
        ];
    }

    public function registerSettings() {
        return [
            'settings' => [
                'label'       => 'Company Settings',
                'description' => 'Manage Company Settings',
                'category'    => 'Company',
                'icon'        => 'icon-credit-card',
                'class'       => 'Monologophobia\Company\Models\Settings',
                'order'       => 400,
                'permissions' => ['monologophobia.company.stripe']
            ]
        ];
    }
}