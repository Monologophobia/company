<?php namespace Monologophobia\Company\Models;

use \October\Rain\Database\Model;
use Monologophobia\Company\Models\Client;
use Monologophobia\Company\Models\Settings;

class Project extends Model {

    /**
     * @var string The database table used by the model.
     */
    public $table = 'monologophobia_company_projects';

    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'      => 'required|min:3',
        'price'     => 'required',
        'client_id' => 'required|min:1'
    ];

    public $belongsTo = [
        'client'    => ['Monologophobia\Company\Models\Client'],
        'salestype' => ['Monologophobia\Company\Models\SalesType', 'key' => 'type'],
        'owner'     => ['Backend\Models\User', 'key' => 'assigned_to']
    ];

    public function listClients() {
        $clients = Client::orderByRaw('(id = 1) DESC, name')->where('active', true)->lists('name', 'id');
        return $clients;
    }

    protected function calculateWeeklyGoalPercentageFromCompletes() {
        $total = $this->where('complete', '=', '1')->whereNull('invoice_id')->sum('price');
        return number_format(($total / intval(Settings::get('target_amount', 700))) * 100, 2);
    }

    public function scopeIsInvoiced($query) {
        //return $query->whereNotNull('invoice_id')->sum('price');
    }

}
