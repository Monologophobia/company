<?php namespace Monologophobia\Company\Models;

use Mail;
use Crypt;
use Backend\Models\BrandSetting;
use \October\Rain\Database\Model;
use Monologophobia\Company\Models\Client;
use Monologophobia\Company\Models\Payment;
use Monologophobia\Company\Models\Settings;

class Invoice extends Model {

    use \October\Rain\Database\Traits\Nullable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'monologophobia_company_invoices';

    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'client_id' => 'required|min:1'
    ];

    protected $dates    = ['paid_date'];
    protected $nullable = ['paid_date', 'payment_reference'];

    public $hasMany = [
        'projects' => ['Monologophobia\Company\Models\Project', 'key' => 'invoice_id'],
        'payments' => ['Monologophobia\Company\Models\Transaction', 'key' => 'invoice_id']
    ];

    public $hasOne = [
        'fee' => ['Monologophobia\Company\Models\Payment', 'key' => 'invoice_id']
    ];

    public $belongsTo = [
        'client' => ['Monologophobia\Company\Models\Client', 'key' => 'client_id']
    ];

    public function beforeCreate() {
        $this->unique_id = \uniqid();
    }

    public function scopeOverdue($query) {
        return $query->orWhereNull('paid_date');
    }

    public function getTotalAmountAttribute() {
        return round($this->projects()->sum('price'), 2);
    }

    public function getAmountDueAttribute() {
        return round($this->total_amount - $this->paid_amount, 2);
    }

    public function isOverdue() {
        if ($this->amount_due > 0) {
            $terms = intval($this->client->terms);
            if ($terms) {
                $overdue_on = strtotime("$this->created_at +$terms days");
                $now        = time();
                if ($now > $overdue_on) return true;
            }
        }
        return false;
    }

    /**
     * Sends a PDF of the invoice to the customer
     * along with a payment link
     * @param String optional message
     */
    public function sendInvoice(string $message) {

        try {

            $email = filter_var($this->client->contact_email, FILTER_SANITIZE_EMAIL);

            // if client is not set to be notified or doesnt have a valid email address, do nothing
            if (!boolval($this->client->notify)) return false;

            // get the page to direct payments to and replace the necessary variables to generate the link
            $page_url     = Settings::get('invoice_payments_page');
            $payment_page = \Cms\Classes\Page::where('url', $page_url)->first();
            $invoice_id   = $payment_page->payinvoice["invoice_id"];
            $unique_id    = $payment_page->payinvoice["unique_id"];
            // strip spaces, {{, and }}
            $invoice_id = str_replace(' ', '', $invoice_id);
            $invoice_id = str_replace('{{', '', $invoice_id);
            $invoice_id = str_replace('}}', '', $invoice_id);
            $unique_id  = str_replace(' ', '', $unique_id);
            $unique_id  = str_replace('{{', '', $unique_id);
            $unique_id  = str_replace('}}', '', $unique_id);
            // replace variables
            $page_url = str_replace($invoice_id, $this->id, $page_url);
            $payment_link = str_replace($unique_id, $this->unique_id, $page_url);
            $payment_link = url($payment_link);

            $user = \BackendAuth::getUser();

            $vars = [
                "invoice"      => $this,
                'name'         => $this->client->name,
                'from_name'    => $user->first_name,
                "contact"      => $this->client->contact,
                'total_amount' => $this->total_amount,
                "email"        => $this->client->email,
                'payment_link' => $payment_link,
                "bank_details" => Settings::get('payment_details'),
                "company_name" => BrandSetting::get("app_name"),
                "custom_message" => $message,
            ];
            Mail::send('monologophobia.company::invoices.invoice', $vars, function($message) use ($vars, $user) {
                $message->to($vars['email'], $vars['name']);
                $message->from($user->email, $user->first_name . ' ' . $user->last_name);
            });

        }
        catch (\Exception $e) {
            \Log::error($e);
        }

    }

    /**
     * Mark the invoice as paid
     * @param integer amount
     * @param integer payment processor fees
     * @param string payment reference
     */
    public function pay(int $amount, int $fees = 0, string $payment_reference) {
        try {
            $date = new \DateTime();
            $this->paid_date   = $date;
            $this->paid_amount = round($amount / 100, 2);
            $this->payment_reference = $payment_reference;
            $this->save();
        }
        catch (\Throwable $e) {
            throw new \Exception("Your payment was successful but we haven't been able to assign it to an invoice. Accounts will sort this out so you don't need to do anything more.");
        }
        if ($fees > 0) {
            try {
                $purchase_type_id = intval(Settings::get('default_payment_type_for_fees'));
                $payment = new Payment;
                $payment->amount     = round($fees / 100, 2);
                $payment->paid_date  = $date;
                $payment->spent_at   = 'Stripe';
                $payment->spent_on   = "Processing Fees";
                $payment->invoice_id = $this->id;
                $payment->purchase_type_id = $purchase_type_id;
                $payment->save();
            }
            catch (\Throwable $e) {
                \Log::error($e);
            }
        }
    }

}
