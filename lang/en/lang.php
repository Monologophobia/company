<?php

return [
    'app' => [
        'currency' => '&pound;'
    ],
    'clients' => [
        'activity' => 'Account Activity',
        'balance'  => 'Balance',
        'ongoing'  => 'Not Yet Invoiced'
    ]
];