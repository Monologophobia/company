// init stripe elements
var form            = document.getElementById('subscribe');
var publishable_key = document.getElementsByName('publishable_key')[0].value;
var stripe      = Stripe(publishable_key);
var elements    = stripe.elements();
var card_errors = document.getElementById('card-errors');

// Create an instance of the card Element
// and add it to an instance of the card Element into the 'card-element' <div>
var card = elements.create('card', {style: {
        base: {
            fontSize: '16px',
            lineHeight: '24px'
        }
    }
});
card.mount('#card-element');

// if an error is detected while the customer is typing, let them know
card.addEventListener('change', function(event) {
    if (event.error) {
        card_errors.textContent = event.error.message;
    }
    else {
        card_errors.textContent = '';
    }
});

// on form submit, submit the card, get a token, then send that token to the server
form.addEventListener('submit', function(e) {
    e.preventDefault();
    // reportValidity() checks the form for validity and reports errors to the user
    if (form.reportValidity()) {
        stripe.createToken(card).then(function(result) {
            if (result.error) {
                // Inform the user if there was an error
                card_errors.textContent = result.error.message;
            }
            else {
                // Send the token to your server
                stripeTokenHandler(result.token);
            }
        });
    };

});

function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    // Submit the form
    //form.submit()
    $(this).request('onSubmit', { data: $(form).serializeArray() });

}
