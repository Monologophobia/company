<?php namespace Monologophobia\Company\Components;

use Log;
use Mail;
use Crypt;
use Request;
use Redirect;
use Exception;
use AjaxException;

use Backend\Models\BrandSetting;
use Monologophobia\Company\Models\Invoice;
use Monologophobia\Company\Models\Settings;

class PayInvoice extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'Pay Invoice',
            'description' => 'Pay specific Invoice number through Stripe'
        ];
    }

    public function defineProperties() {
        return [
            'invoice_id' => [
                'title' => 'Encrypted Invoice ID',
                'type'  => 'string'
            ],
            'unique_id' => [
                 'title' => 'Encrypted Unique Invoice ID',
                 'type'  => 'string'
            ]
        ];
    }

    public function onRun() {

        try {

            $invoice_id = intval($this->property('invoice_id'));
            $unique_id  = $this->property('unique_id');
            $this->page['invoice'] = Invoice::where('unique_id', $unique_id)->findOrFail($invoice_id);

            $this->addCss('/plugins/monologophobia/company/assets/css/invoice.css');
            $this->page['settings'] = Settings::instance();
            $this->page['brand']    = BrandSetting::instance();

            $this->page['publishable_key'] = Settings::getStripeKeys()['publishable_key'];

        }
        catch (\Throwable $e) {
            \Log::error($e);
            return $this->make404();
        }

    }

    private function make404() {
        return \Response::make($this->controller->run('404'), 404);
    }

    public function onPay() {
        try {
            $invoice_id = intval(post('id'));
            $unique_id  = post('unique_id');
            $invoice    = Invoice::where('unique_id', $unique_id)->findOrFail($invoice_id);
            if ($invoice->amount_due <= 0) {
                throw new \Exception("Invoice already paid");
            }
            $return = $this->createOrConfirmPaymentIntent($invoice, post('payment_method_id'), post('payment_intent_id'));
            if ($return->requires_action || $return->error) {
                return \Response::json($return);
            }

            // now we need to get the balance transaction to get the stripe fee
            $txn         = $return->payment_intent->charges->data[0]->balance_transaction;
            $transaction = \Stripe\BalanceTransaction::retrieve($txn);
            $amount      = $transaction->amount;
            $fee         = $transaction->fee;

            // ace. now record the transaction as complete
            $invoice->pay($amount, $fee, $return->payment_intent->id);
            \Flash::success("Your payment has been made successfully. Thank you.");
            return \Redirect::refresh();

        }
        catch (\Throwable $e) {
            throw new AjaxException($e->getMessage());
        }
    }

    /**
     * Create or confirm a Payment Intent
     * @param Invoice
     * @param String optional payment_method_id
     * @param String optional payment_intent_id
     * @return Object {requires_action: boolean, payment_intent_client_secret: String, error: bool, message: optional error string}
     */
    private function createOrConfirmPaymentIntent($invoice, $method = false, $intent = false) {

        if (!$method && !$intent) throw new Exception('Method or Intent must be specified');

        $live = boolval(Settings::get('live'));
        $key  = ($live ? Settings::get('stripe_secret') : Settings::get('stripe_sandbox_secret'));
        \Stripe\Stripe::setApiKey($key);

        $payment_intent = false;
        $return = ['requires_action' => false, 'payment_intent_client_secret' => false, 'error' => false, 'payment_intent' => false];

        try {

            // Do intent retrieval first as that is more important on the second round trip
            if ($intent) {
                $payment_intent = \Stripe\PaymentIntent::retrieve($intent);
                $payment_intent->confirm();
            }
            else if ($method) {

                $payment_array = [
                    'amount'      => $invoice->amount_due * 100,
                    'currency'    => 'GBP',
                    'confirm'     => true,
                    'description' => "Invoice " . $invoice->id,
                    'receipt_email'  => $invoice->client->email,
                    'payment_method' => $method,
                    'confirmation_method' => 'manual',
                ];

                $payment_intent = \Stripe\PaymentIntent::create($payment_array);

            }

            if ($payment_intent->status == 'requires_action' && $payment_intent->next_action->type == 'use_stripe_sdk') {
                $return['requires_action'] = true;
                $return['payment_intent_client_secret'] = $payment_intent->client_secret;
            }
            else if ($payment_intent->status == 'succeeded') {
                $return['payment_intent'] = $payment_intent;
            }

        }
        catch (\Stripe\Error\Base $e) {
            $return['error']   = true;
            $return['message'] = $e->getMessage();
        }

        return (object) $return;

    }

}
