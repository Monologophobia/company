<?php namespace Monologophobia\Company\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Monologophobia\Company\Models\Project;
use Monologophobia\Company\Models\SalesType;

class Overview extends ReportWidgetBase {

    public function render() {

        try {
            $this->loadData();
        }
        catch (Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        $this->addJs('/plugins/monologophobia/company/reportwidgets/assets/js/widgets.js');
        $this->addCss('/plugins/monologophobia/company/reportwidgets/assets/css/widgets.css');

        return $this->makePartial('overview');

    }

    public function defineProperties() {
        return [
            'title' => [
                'title'             => 'Label',
                'default'           => 'Company Overview',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'backend::lang.dashboard.widget_title_error',
            ]
        ];
    }

    protected function loadData() {
        $this->vars['ongoing']  = Project::where('complete', '=', '0')->whereNull('invoice_id')->count();
        $this->vars['complete'] = Project::where('complete', '=', '1')->whereNull('invoice_id')->count();
        $this->vars['goal']     = Project::calculateWeeklyGoalPercentageFromCompletes();
        $this->vars['types']    = SalesType::get();
    }

}