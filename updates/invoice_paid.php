<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class InvoicePaid extends Migration {

    public function up() {

        Schema::table('monologophobia_company_invoices', function($table) {
            $table->boolean('paid')->default(false);
        });

    }

    public function down() {
        Schema::table('monologophobia_company_invoices', function($table) {
            $table->dropColumn('paid');
        });
    }

}