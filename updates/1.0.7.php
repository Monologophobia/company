<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointZeroPointSeven extends Migration {

    public function up() {
        Schema::table('monologophobia_company_payments', function($table) {
            $table->text('spent_at')->nullable();
            $table->text('spent_on')->nullable();
        });
    }

    public function down() {
        Schema::table('monologophobia_company_payments', function($table) {
            $table->dropColumn('spent_at');
            $table->dropColumn('spent_on');
        });
    }

}