<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointZeroPointEight extends Migration {

    public function up() {

        Schema::table('monologophobia_company_clients', function($table) {
            $table->dropColumn('password');
        });
        Schema::table('monologophobia_company_clients', function($table) {
            $table->dropColumn('activation_code');
        });
        Schema::table('monologophobia_company_clients', function($table) {
            $table->dropColumn('persist_code');
        });
        Schema::table('monologophobia_company_clients', function($table) {
            $table->dropColumn('reset_password_code');
        });
        Schema::table('monologophobia_company_clients', function($table) {
            $table->dropColumn('permissions');
        });

    }

    public function down() {
        Schema::table('monologophobia_company_clients', function($table) {
            $table->string('password');
            $table->string('activation_code')->nullable()->index();
            $table->string('persist_code')->nullable();
            $table->string('reset_password_code')->nullable()->index();
            $table->text('permissions')->nullable();
        });
    }

}