<?php namespace Monologophobia\Company\Updates;

use Seeder;
use Monologophobia\Company\Models\SaleTypesModel;

class SeedCompanyTables extends Seeder {

    public function run() {

        $sales_type = SaleTypesModel::create([
                        'name' => 'Artwork',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

    }

}