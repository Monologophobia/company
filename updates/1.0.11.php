<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointZeroPointEleven extends Migration {

    public function up() {

        Schema::table('monologophobia_company_invoices', function($table) {
            $table->dateTime('paid_date')->nullable();
        });

    }

    public function down() {
        Schema::table('monologophobia_company_invoices', function($table) {
            $table->dropColumn('paid_date');
        });
    }

}