<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointZeroPointTwelve extends Migration {

    public function up() {

        Schema::table('monologophobia_company_payments', function($table) {
            $table->integer('invoice_id')->nullable();
        });

    }

    public function down() {
        Schema::table('monologophobia_company_payments', function($table) {
            $table->dropColumn('invoice_id');
        });
    }

}