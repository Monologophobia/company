<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddDate extends Migration {

    public function up() {

        Schema::table('monologophobia_company_payments', function($table) {
            $table->date('paid_date')->nullable();
        });

    }

    public function down() {
        Schema::table('monologophobia_company_payments', function($table) {
            $table->dropColumn('paid_date');
        });
    }

}