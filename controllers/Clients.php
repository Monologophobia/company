<?php namespace Monologophobia\Company\Controllers;

use Flash;
use BackendAuth;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Company\Models\Client;
use Lang;

class Clients extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.company.clients'];

    public $bodyClass = 'compact-container';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Company', 'company', 'clients');
    }

    public function update($recordId, $context = null) {

        $client = Client::find($recordId);

        // set up the variables we need to display to the user
        $this->vars['balance']       = $client->invoiced_projects->sum('price');
        $this->vars['projects']      = $client->projects;
        $this->vars['ongoing']       = 0;
        $this->vars['ongoing_total'] = 0;

        // iterate over the projects
        foreach ($this->vars['projects'] as $project) {
            // if the project has been invoiced
            if ($project->invoice_id) {
                
            }
            // if the project has not been invoiced
            else {
                // add up the total of the ongoing projects
                $this->vars['ongoing']       += 1;
                $this->vars['ongoing_total'] += $project->price;
            }
        }

        return $this->asExtension('FormController')->update($recordId, $context);

    }

}