<?php namespace Monologophobia\Company\Controllers;

use Flash;
use Backend;
use BackendAuth;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Company\Models\Project;
use Monologophobia\Company\Models\Invoice;

class Projects extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.company.projects'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Company', 'company', 'projects');
    }

    public function index () {
        $this->vars['ongoing']  = Project::where('complete', '=', '0')->whereNull('invoice_id')->count();
        $this->vars['complete'] = Project::where('complete', '=', '1')->whereNull('invoice_id')->count();
        $this->vars['goal']     = Project::calculateWeeklyGoalPercentageFromCompletes();
        $this->asExtension('ListController')->index();
    }

    public function listInjectRowClass($record, $definition = null) {
        if ($record->invoice_id) {
            return 'invoiced';
        }
        if ($record->complete == 1) {
            return 'complete';
        }
        if ($record->complete == 2) {
            return 'suspended';
        }
        return 'ongoing';
    }

    public function onCreateQuickProject() {
        return $this->listRefresh();
    }

    public function index_onDelete() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            $errors = false;
            foreach ($checkedIds as $id) {
                $project = Project::find($id);
                if ($project && !$project->invoice_id) {
                    $project->delete();
                } else {
                    $errors = true;
                }
            }

            if (!$errors) {
                Flash::success('Deleted Successfully');
            } else {
                Flash::error('Some items have already been invoiced and can\'t be deleted');
            }

        }
        else {
            Flash::error('Couldn\'t find Project IDs');
        }
        return $this->listRefresh();
    }

    public function index_onDuplicate() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $id) {
                $project = Project::find($id)->replicate();
                $project->invoice_id = null;
                $project->created_at = date('Y-m-d H:i:s');
                $project->updated_at = date('Y-m-d H:i:s');
                $project->save();
            }
            Flash::success('Duplicated');
        }
        else {
            Flash::error('Couldn\'t find Project IDs');
        }
        return $this->listRefresh();
    }
    
    public function index_onInvoice() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            $invoices  = [];

            foreach ($checkedIds as $id) {

                // Check if the item has already been invoiced
                $project = Project::find($id);
                if ($project->invoice_id) {
                    Flash::error('Project ' . $id . ' already invoiced');
                }

                // Collate the invoices by client
                if (!isset($invoices[$project->client_id])) $invoices[$project->client_id] = [];
                array_push($invoices[$project->client_id], $project->id);

            }

            // Create invoices
            foreach ($invoices as $client_id => $invoice_items) {
                // First, create an invoice record
                $invoice = new Invoice;
                $invoice->client_id = $client_id;
                $invoice->save();
                // then update the project records with the returned invoice id
                foreach ($invoice_items as $item) {
                    $project = Project::find($item);
                    $project->invoice_id = $invoice->id;
                    $project->complete   = 1;
                    $project->save();
                }
            }
            Flash::success('Projects Invoiced');
            return Backend::redirect('monologophobia/company/invoices');
        }
        else {
            Flash::error('Couldn\'t find Project IDs');
        }
        return $this->listRefresh();
    }
}