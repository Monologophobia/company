<?php namespace Monologophobia\Company\Controllers;

use Flash;
use File;
use BackendAuth;
use BackendMenu;
use Backend\Classes\Controller;

define('STDIN',fopen("php://stdin","r"));

class Files extends Controller {    

    private $app_name = "The Quill Design Shared Files";
    private $credentials_path;
    private $secret_path;
    private $client;

    public $requiredPermissions = ['monologophobia.company.files'];

    //public $bodyClass = 'compact-container';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Company', 'company', 'files');
        //$this->client = $this->startGoogle();
    }

    public function index () {

        //$service = new \Google_Service_Drive($this->client);
        //$this->vars['files'] = $service->files->listFiles();
        //$this->vars['directories'] = $service->children->listChildren('root');

    }

    // taken from https://developers.google.com/drive/web/quickstart/php
    private function startGoogle() {

        $credentials_path = base_path('plugins/monologophobia/company/security/google_authorisation.json');
        $secret_path      = base_path('plugins/monologophobia/company/security/google_secret.json');

        require_once base_path('plugins/monologophobia/company/libraries/google-api-php-client/src/Google/autoload.php');

        $client = new \Google_Client();
        $client->setApplicationName($this->app_name);
        $client->setAuthConfigFile($secret_path);
        $client->setScopes("https://www.googleapis.com/auth/drive");

        // Load previously authorized credentials from a file.
        if (file_exists($credentials_path)) {
            $accessToken = file_get_contents($credentials_path);
        }
        else {

            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->authenticate($authCode);

            // Store the credentials to disk.
            if(!file_exists(dirname($credentials_path))) {
                mkdir(dirname($credentials_path), 0700, true);
            }

            file_put_contents($credentials_path, $accessToken);
            printf("Credentials saved to %s\n", $credentials_path);

        }

        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->refreshToken($client->getRefreshToken());
            file_put_contents($credentials_path, $client->getAccessToken());
        }

        return $client;

    }

}